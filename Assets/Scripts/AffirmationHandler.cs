using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class AffirmationHandler : MonoBehaviour
{
    public int cloudsDestroyed;
    public TMP_Text firstAffirmation;
    public TMP_Text secondAffirmation;
    public TMP_Text thirdAffirmation;

    private bool playedFirstAffirmation;
    private bool playedSecondAffirmation;
    private bool playedThirdAffirmation;

    private void Start()
    {
        playedFirstAffirmation = false;
        playedSecondAffirmation = false;
        playedThirdAffirmation = false;            
    }
    private void OnEnable()
    {
        EventManager.onCloudDestroyed += IncreaseCloudInt;
    }

    private void OnDisable()
    {
        EventManager.onCloudDestroyed -= IncreaseCloudInt;
    }
    private void IncreaseCloudInt()
    {
        cloudsDestroyed += 1;
        if(cloudsDestroyed >= 3)
        {
            if(playedFirstAffirmation == false)
            {
                PlayFirstAffirmation();
                playedFirstAffirmation = true;
            }           
            
        }
        if(cloudsDestroyed >= 6)
        {
            if (playedSecondAffirmation == false)
            {
                PlaySecondAffirmation();
                playedSecondAffirmation = true;
            }
            
            
        }
        if(cloudsDestroyed >= 9)
        {
            PlaythirdAffirmation();
        }
    }

    private void PlayFirstAffirmation()
    {
        Debug.Log("Play first affirmation");
        StartCoroutine(FirstAffirmation());
    }

    public IEnumerator FirstAffirmation()
    {
        Color objectColour = firstAffirmation.color;
        float fadeAmount;
        float fadeSpeed = 1f;

        while (firstAffirmation.color.a < 1)
        {
            fadeAmount = objectColour.a + (fadeSpeed * Time.deltaTime);

            objectColour = new Color(objectColour.r, objectColour.g, objectColour.b, fadeAmount);
            firstAffirmation.color = objectColour;
            yield return null;
        }

        yield return new WaitForSeconds(1);

        while (firstAffirmation.color.a > 0)
        {
            fadeAmount = objectColour.a - (fadeSpeed * Time.deltaTime);

            objectColour = new Color(objectColour.r, objectColour.g, objectColour.b, fadeAmount);
            firstAffirmation.color = objectColour;
            yield return null;
        }        
    }

    private void PlaySecondAffirmation()
    {
        Debug.Log("Play second affirmation");
        StartCoroutine(SecondAffirmation());
    }

    public IEnumerator SecondAffirmation()
    {
        Color objectColour = secondAffirmation.color;
        float fadeAmount;
        float fadeSpeed = 1f;

        while (secondAffirmation.color.a < 1)
        {
            fadeAmount = objectColour.a + (fadeSpeed * Time.deltaTime);

            objectColour = new Color(objectColour.r, objectColour.g, objectColour.b, fadeAmount);
            secondAffirmation.color = objectColour;
            yield return null;
        }

        yield return new WaitForSeconds(2);

        while (secondAffirmation.color.a > 0)
        {
            fadeAmount = objectColour.a - (fadeSpeed * Time.deltaTime);

            objectColour = new Color(objectColour.r, objectColour.g, objectColour.b, fadeAmount);
            secondAffirmation.color = objectColour;
            yield return null;
        }
    }

    private void PlaythirdAffirmation()
    {
        Debug.Log("Play third affirmation");
        StartCoroutine(ThirdAffirmation());
    }

    public IEnumerator ThirdAffirmation()
    {
        Color objectColour = thirdAffirmation.color;
        float fadeAmount;
        float fadeSpeed = 1f;

        while (thirdAffirmation.color.a < 1)
        {
            fadeAmount = objectColour.a + (fadeSpeed * Time.deltaTime);

            objectColour = new Color(objectColour.r, objectColour.g, objectColour.b, fadeAmount);
            thirdAffirmation.color = objectColour;
            yield return null;
        }

        yield return new WaitForSeconds(2);

        while (thirdAffirmation.color.a > 0)
        {
            fadeAmount = objectColour.a - (fadeSpeed * Time.deltaTime);

            objectColour = new Color(objectColour.r, objectColour.g, objectColour.b, fadeAmount);
            thirdAffirmation.color = objectColour;
            yield return null;
        }
    }
}
