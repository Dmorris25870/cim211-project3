using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DragController : MonoBehaviour, IDragHandler, IEndDragHandler
{

    private bool canDestroy;

    // Start is called before the first frame update
    void Start()
    {
        canDestroy = false;
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void OnDrag(PointerEventData eventData)
    {
        transform.position = eventData.position;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if(canDestroy)
        {
            Destroy(gameObject);
            EventManager.onCloudDestroyed?.Invoke();
        }
    }

    

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("border"))
        {
            Debug.Log("can destroy " + gameObject.name);
            Color color = collision.gameObject.GetComponent<Image>().color;
            color.a = 0.75f;
            collision.gameObject.GetComponent<Image>().color = color;
            canDestroy = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("border"))
        {
            Debug.Log("cannot destroy " + gameObject.name);
            Color color = collision.gameObject.GetComponent<Image>().color;
            color.a = 1f;
            collision.gameObject.GetComponent<Image>().color = color;
            canDestroy = false;
        }
    }

}
